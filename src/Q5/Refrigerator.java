package Q5;

import java.util.ArrayList;

public class Refrigerator {
	int size;
	ArrayList<String> things;
	
	public Refrigerator(int s){
		things = new ArrayList<String>();
		size = s;
	}
	
	public void put(String stuff) throws FullException{
		if(things.size()<size){
			things.add(stuff);
		}
		else{
			throw new FullException();
		}
	}
	
	public String takeOut(String stuff){
		for(String item : things){
			if(item.equals(stuff)){
				things.remove(things.indexOf(item));
				return "";
			}
			}
		return null;
		}
	
	public String toString(){
		String s="";
		for(String item : things){
			s +="\n"+item;
		}
		return s;
		
	}
}
