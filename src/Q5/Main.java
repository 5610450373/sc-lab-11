package Q5;

public class Main {
	public static void main(String[] args){
		try{
		Refrigerator re = new Refrigerator(5);
		re.put("A");
		re.put("B");
		re.put("C");
		re.put("D");
		re.put("AA");
		System.out.println("1");
		re.takeOut("A");
		System.out.println("2");
		re.put("E");
		re.put("BB");
		System.out.println("3");
		
		}
		catch(FullException e){
			System.err.println("Limited");
		}
	}
}
